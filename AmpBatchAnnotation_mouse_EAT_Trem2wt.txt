	Amp.Batch.ID	Seq.Batch.ID	Batch.Set.ID	Owner	Description	Diet	FAT	Age	mouse.ID	Perfused	Genotype
1	AB2207	SB115	adipo_CD45+	Diego	HFD EAT perf. mouse #1 #4	HFD	EAT	18	3	1	WT
2	AB2208	SB115	adipo_CD45+	Diego	NC EAT perf. mouse #1 #9	NC	EAT	18	4	1	WT
3	AB2364	SB122	adipo_CD45+	Diego	HFD perfused mouse 2 EAT #12	HFD	EAT	18	5	1	WT
4	AB2365	SB122	adipo_CD45+	Diego	HFD perfused mouse 2 EAT #13	HFD	EAT	18	5	1	WT
5	AB2366	SB122	adipo_CD45+	Diego	HFD perfused mouse 2 EAT #14	HFD	EAT	18	5	1	WT
6	AB2367	SB122	adipo_CD45+	Diego	HFD perfused mouse 2 EAT #15	HFD	EAT	18	5	1	WT
7	AB2368	SB122	adipo_CD45+	Diego	HFD perfused mouse 2 EAT #16	HFD	EAT	18	5	1	WT
8	AB2388	SB123	adipo_CD45+	Diego	NC perfused EAT mouse #2 #4	NC	EAT	18	6	1	WT
9	AB2389	SB123	adipo_CD45+	Diego	NC perfused EAT mouse #2 #5	NC	EAT	18	6	1	WT
10	AB2390	SB124	adipo_CD45+	Diego	HFD EAT perf. #1 #2	HFD	EAT	18	3	1	WT
11	AB2391	SB124	adipo_CD45+	Diego	HFD EAT perf. #1 #3	HFD	EAT	18	3	1	WT
12	AB2392	SB124	adipo_CD45+	Diego	HFD EAT perf. #1 #5	HFD	EAT	18	3	1	WT
13	AB2393	SB124	adipo_CD45+	Diego	NC EAT perf. #1 #6	NC	EAT	18	4	1	WT
14	AB2394	SB124	adipo_CD45+	Diego	NC EAT perf. #1 #7	NC	EAT	18	4	1	WT
15	AB2395	SB124	adipo_CD45+	Diego	NC EAT perf. #1 #8	NC	EAT	18	4	1	WT
16	AB2400	SB124	adipo_CD45+	Diego	NC EAT perf. #2 #6	NC	EAT	18	6	1	WT
17	AB2401	SB124	adipo_CD45+	Diego	NC EAT perf. #2 #7	NC	EAT	18	6	1	WT
18	AB2402	SB124	adipo_CD45+	Diego	NC EAT perf. #2 #8	NC	EAT	18	6	1	WT
19	AB2445	SB127	adipo_CD45+	Diego	HFD young EAT perf. #2	HFD	EAT	6	7	1	WT
20	AB2446	SB127	adipo_CD45+	Diego	HFD young EAT perf. #3	HFD	EAT	6	7	1	WT
21	AB2447	SB127	adipo_CD45+	Diego	HFD young EAT perf. #4	HFD	EAT	6	7	1	WT
22	AB2448	SB127	adipo_CD45+	Diego	HFD young EAT perf. #5	HFD	EAT	6	7	1	WT
23	AB2449	SB127	adipo_CD45+	Diego	HFD young EAT perf. #6	HFD	EAT	6	7	1	WT
24	AB2452	SB127	adipo_CD45+	Diego	NC young EAT perf. #10	NC	EAT	6	8	1	WT
25	AB2453	SB127	adipo_CD45+	Diego	NC young EAT perf. #11	NC	EAT	6	8	1	WT
26	AB2454	SB127	adipo_CD45+	Diego	NC young EAT perf. #12	NC	EAT	6	8	1	WT
27	AB2455	SB127	adipo_CD45+	Diego	NC young EAT perf. #13	NC	EAT	6	8	1	WT
28	AB2456	SB127	adipo_CD45+	Diego	NC young EAT perf. #14	NC	EAT	6	8	1	WT
29	AB2854	SB145	fat_cd45+	Diego	NC EAT int male #2 	NC	EAT	12	16	1	WT
30	AB2855	SB145	fat_cd45+	Diego	HFD EAT int male #1 	HFD	EAT	12	13	1	WT
31	AB2856	SB145	fat_cd45+	Diego	HFD EAT int male #1 	HFD	EAT	12	13	1	WT
32	AB2857	SB145	fat_cd45+	Diego	HFD EAT int male #2 #27	HFD	EAT	12	14	1	WT
33	AB2920	SB149	adipo_CD45+	Diego	NC EAT int male #1 #1	NC	EAT	12	15	1	WT
34	AB2921	SB149	adipo_CD45+	Diego	NC EAT int male #1 #2	NC	EAT	12	15	1	WT
35	AB2922	SB149	adipo_CD45+	Diego	NC EAT int male #1 #3	NC	EAT	12	15	1	WT
36	AB2923	SB149	adipo_CD45+	Diego	NC EAT int male #2 #4	NC	EAT	12	16	1	WT
37	AB2924	SB149	adipo_CD45+	Diego	NC EAT int male #2 #5	NC	EAT	12	16	1	WT
38	AB2925	SB149	adipo_CD45+	Diego	HFD EAT int male #1 #9	HFD	EAT	12	13	1	WT
39	AB2926	SB149	adipo_CD45+	Diego	HFD EAT int male #1 #10	HFD	EAT	12	13	1	WT
40	AB2927	SB149	adipo_CD45+	Diego	HFD EAT int male #1 #11	HFD	EAT	12	13	1	WT
41	AB2928	SB149	adipo_CD45+	Diego	HFD EAT int male #2 #12	HFD	EAT	12	14	1	WT
42	AB2929	SB149	adipo_CD45+	Diego	HFD EAT int male #2 #13	HFD	EAT	12	14	1	WT
43	AB2930	SB149	adipo_CD45+	Diego	HFD EAT int male #2 #15	HFD	EAT	12	14	1	WT
44	AB2931	SB149	adipo_CD45+	Diego	HFD EAT int male #2 #16	HFD	EAT	12	14	1	WT
45	AB3127	SB158	earlyHFD_CD45+	Diego	EAT NC1  #1	NC	EAT	6	98	1	WT
46	AB3128	SB158	earlyHFD_CD45+	Diego	EAT NC1  #2	NC	EAT	6	98	1	WT
47	AB3129	SB158	earlyHFD_CD45+	Diego	EAT NC1  #3	NC	EAT	6	98	1	WT
48	AB3130	SB158	earlyHFD_CD45+	Diego	EAT HFD1  #5	HFD	EAT	6	98	1	WT
49	AB3131	SB158	earlyHFD_CD45+	Diego	EAT HFD1  #6	HFD	EAT	6	98	1	WT
50	AB3132	SB158	earlyHFD_CD45+	Diego	EAT HFD1  #7	HFD	EAT	6	98	1	WT
51	AB5919	SB299	adipo_CD45+	Diego	12 weeks HFD EAT mouse_1 plate_1	HFD	EAT	12	101	1	WT
52	AB5920	SB299	adipo_CD45+	Diego	12 weeks HFD EAT mouse_1 plate_2	HFD	EAT	12	101	1	WT
53	AB5921	SB299	adipo_CD45+	Diego	12 weeks HFD EAT mouse_1 plate_3	HFD	EAT	12	101	1	WT
54	AB5922	SB299	adipo_CD45+	Diego	12 weeks HFD EAT mouse_1 plate_4	HFD	EAT	12	201	1	WT
55	AB5923	SB299	adipo_CD45+	Diego	12 weeks NC EAT mouse_1 plate_5	NC	EAT	12	201	1	WT
56	AB5924	SB299	adipo_CD45+	Diego	12 weeks NC EAT mouse_1 plate_6	NC	EAT	12	201	1	WT
57	AB5925	SB299	adipo_CD45+	Diego	12 weeks NC EAT mouse_1 plate_8	NC	EAT	12	102	1	WT
58	AB5926	SB299	adipo_CD45+	Diego	12 weeks HFD EAT mouse_2 plate_9	HFD	EAT	12	102	1	WT
59	AB5927	SB299	adipo_CD45+	Diego	12 weeks HFD EAT mouse_2 plate_10	HFD	EAT	12	102	1	WT
60	AB5928	SB299	adipo_CD45+	Diego	12 weeks HFD EAT mouse_2 plate_11	HFD	EAT	12	102	1	WT
61	AB5929	SB299	adipo_CD45+	Diego	12 weeks HFD EAT mouse_2 plate_12	HFD	EAT	12	202	1	WT
62	AB5930	SB299	adipo_CD45+	Diego	12 weeks NC EAT mouse_2 plate_14	NC	EAT	12	202	1	WT
63	AB5931	SB299	adipo_CD45+	Diego	12 weeks NC EAT mouse_2 plate_15	NC	EAT	12	202	1	WT
64	AB5932	SB299	adipo_CD45+	Diego	12 weeks NC EAT mouse_2 plate_16	NC	EAT	12	202	1	WT
65	AB5933	SB300	adipo_CD45+	Diego	18 weeks HFD mouse_1_1	HFD	EAT	18	103	1	WT
66	AB5934	SB300	adipo_CD45+	Diego	18 weeks HFD mouse_1_2	HFD	EAT	18	103	1	WT
67	AB5935	SB300	adipo_CD45+	Diego	18weeks HFD m_1 p_3	HFD	EAT	18	103	1	WT
68	AB5936	SB300	adipo_CD45+	Diego	18weeks HFD m_1 p_4	HFD	EAT	18	103	1	WT
69	AB5937	SB300	adipo_CD45+	Diego	18 weeks NC mouse_1_7	NC	EAT	18	203	1	WT
70	AB5938	SB300	adipo_CD45+	Diego	18 weeks NC mouse_1_8	NC	EAT	18	203	1	WT
71	AB5939	SB300	adipo_CD45+	Diego	18 weeks NC mouse_1_9	NC	EAT	18	203	1	WT
72	AB5940	SB300	adipo_CD45+	Diego	18 weeks HFD mouse_2_11	HFD	EAT	18	104	1	WT
73	AB5941	SB300	adipo_CD45+	Diego	18 weeks HFD mouse_2_12	HFD	EAT	18	104	1	WT
74	AB5942	SB300	adipo_CD45+	Diego	18 weeks HFD mouse_2_15	HFD	EAT	18	104	1	WT
75	AB5943	SB300	adipo_CD45+	Diego	18weeks HFD m_2 p_14	HFD	EAT	18	104	1	WT
76	AB5944	SB300	adipo_CD45+	Diego	18 weeks NC mouse_2_17	NC	EAT	18	204	1	WT
77	AB5945	SB300	adipo_CD45+	Diego	18 weeks NC mouse_2_18	NC	EAT	18	204	1	WT
78	AB5946	SB300	adipo_CD45+	Diego	18 weeks NC mouse_2_19	NC	EAT	18	204	1	WT