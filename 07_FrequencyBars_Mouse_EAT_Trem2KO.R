# R Version 3.5.0

# Setup ####

# load all relvant libraries
require(metacell) 
require(stringr)
require(ggplot2)
# set working directory to home directory
setwd("~")

# create saved_work directory and initialize it to save data objects
scdb_init("saved_work/", force_reinit=T)
id <- "Trem2KOclean"
scfigs_init("results/")

mat = scdb_mat(id)
cell_stats = mat@cell_metadata
mc2 = scdb_mc2d(id)
mc = scdb_mc(id)  

# load colorizing table for identified groups of cells ("metacells")
marks_colors = read.delim("mc_colorize_mouse_EAT_Trem2wt.txt", sep="\t", h=T, stringsAsFactors=F)
mc_colorize(id, marker_colors=marks_colors)


# create data frame for single cells
fat <- data.frame(mc2@sc_x, mc2@sc_y,
                  cell_stats$FAT[which(rownames(cell_stats) %in% rownames(mc2@sc_x))],
                  cell_stats$Diet[which(rownames(cell_stats) %in% rownames(mc2@sc_x))],
                  cell_stats$Genotype[which(rownames(cell_stats) %in% rownames(mc2@sc_x))],
                  cell_stats$Perfused[which(rownames(cell_stats) %in% rownames(mc2@sc_x))],
                  cell_stats$Age[which(rownames(cell_stats) %in% rownames(mc2@sc_x))],
                  cell_stats$mouse.ID[which(rownames(cell_stats) %in% rownames(mc2@sc_x))],
                  cell_stats$batch_set_id[which(rownames(cell_stats) %in% rownames(mc2@sc_x))],
                  cell_stats$amp_batch_id[which(rownames(cell_stats) %in% rownames(mc2@sc_x))],
                  mc@mc[which(names(mc@mc) %in% rownames(mc2@sc_x))])  
colnames(fat) <- c("x", "y", "FAT","Diet", "Genotype","Perfused", "Age", "Mouse","Batch.Set.ID","Amp.Batch.ID", "Cluster")
fat$CellID <- rownames(fat)
fat$Age <- as.numeric(as.character(fat$Age))
dim(fat)
#[1] 10042    12

# load manual metacell annotation
manan <- read.delim(file="MetacellManualAnnotation_Trem2KO.txt", stringsAsFactors = F)
manan$Type[manan$Type == ""] <- NA
manancolor <- merge(manan,mc@color_key, by.x="Type", by.y="group", all=TRUE)
manancolor1 <- unique(manancolor[c("Type", "x", "color")])

fat.scatter2 <- merge(fat,na.omit(manancolor1), by.x="Cluster", by.y="x", all=TRUE)



# load custom ggplot2 theme 
require(ggplot2)
source("functions/theme_publa.R")

# sort and annotate data set
fat.scatter2 <- na.omit(fat.scatter2)
fat.scatter2$Type <- factor(fat.scatter2$Type, levels = c("Mon","Mac", "B","Plasma","Mast", "Neut",
                                                          "cDC1","cDC2", #"pDC", 
                                                          "Treg","CD4 T","CD8 T", "NK", "ILC2"))
fat.scatter2 <- fat.scatter2[with(fat.scatter2, order(Type)), ]

labels <- unique(fat.scatter2$Type)
cellcols <- unique(fat.scatter2$color)


dotsize <- 0.5
figwidth <- 200/2

library(dplyr)
library(ggplot2)
source("/home/labs/amit/lorenzad/Coding/R/Functions/Own/theme_publa.R")


# Frequency bars, Figure 4B ####

fat <- fat.scatter2  

# rearrange
fat$Diet <- factor(fat$Diet, levels=c("NC","HFD"))
fat$Genotype <- factor(fat$Genotype, levels=c("WT_Littermates", "Trem2KO"), labels= c("WT", "Trem2KO"))

fat.plot <- fat

g <- ggplot(fat.plot, aes(Genotype)) +
  labs(title  = NULL, # plot title
       x      = NULL, # x-axis label
       y      = "Cell type distribution") + # y-axis label
  geom_bar(aes(fill=Type), position = "fill", color="black", size=0.25) +
  scale_y_continuous(expand = c(0,0), limits = c(0, 1.025)) +
  scale_fill_manual(name=NULL, labels = labels, guide="legend", values=cellcols) +
  facet_wrap(~Diet, nrow = 1, scales = "free") +
  theme_publa(base_size = 8)  +
  theme(legend.background = element_blank(),
        axis.ticks = element_line(colour="black"),
        axis.line.y = element_line(colour = "black", size=0.5))+
  guides(fill=guide_legend(keywidth=2.5,keyheight=2.5,default.unit="mm"))

g

figwidth <- 200/2.5
ggsave(filename = paste0("results/",id,"_FrequencyBars.pdf"),
       width=figwidth, height=figwidth*9/16, units="mm", dpi=150, useDingbats=FALSE)

