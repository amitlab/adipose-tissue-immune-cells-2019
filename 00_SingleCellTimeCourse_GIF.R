# R version 3.6.0 (2019-04-26)

# set working directory
setwd("~/Documents/GitHub/Adipose-tissue-immune-cells/")

# load single cell matrix
fat <- read.csv(file="SingleCellMatrix.csv")


# set normal chow (NC) to time point 0 of high-fat diet (HFD) feeding
fat$Age[fat$Diet == "NC"] <- 0

# look only at weeks 6 and 12 as major transition occurs there
fat <- fat[-which(fat$Age %in% c(0,18)),]


fat$condition <- paste0(fat$Age)
# downsample to equal number of cells per condition
minnumbcells <- min(sapply(c(as.character(unique(fat$condition))), function(x) length(rownames(fat[fat$condition==x,]))))
set.seed(22)
plotcells <-  as.vector(sapply(c(as.character(unique(fat$condition))), function(x) sample(rownames(fat[fat$condition==x,]), 
                                                                                          size=minnumbcells, replace=F, prob=NULL )))

fat.plotall <- fat[,c("x","y")]

# rearrange
fat$Type <- factor(fat$Type, levels = c("Mon","Mac", "B","Plasma","Mast", "Neut",
                                                          "cDC1","cDC2", "pDC", "Naive T",
                                                          "Treg","CD4 T","CD8 T", "NK", "ILC"))

fat <- fat[with(fat, order(Type)), ]

labels <- (unique(fat[,]$Type))
cellcols <- as.character(unique(fat[,]$color)) 

# rendering parameter for animation needs to be numeric under these settings
fat$time <- as.numeric(fat$condition)
                                                                                                
# set graphics parameters
figtextsize <- 16
dotsize <- 1.5
figwidth <- 210



# ggplot2 needs to be down-graded for the current gganimate version to work

# install.packages('devtools')
# devtools::install_github('thomasp85/gganimate')
# library(gapminder)
# require(devtools)
# install_version("ggplot2", version = "3.1.1", repos = "http://cran.us.r-project.org")

# load required packages for the graphics
require(gganimate)
require(ggplot2)
source("functions/theme_publa.R")

p1 <-  
    ggplot(NULL, aes(x=x, y=y)) + 
    geom_point(data=fat.plotall, colour="grey89", fill="grey89", size=dotsize, shape=21, stroke = 0.05*2/4) +
    geom_point(data=fat[plotcells
      ,], aes(fill=factor(Type), colour=factor(Type)), size=dotsize, shape=21, stroke = 0.05*2/4) +
    scale_fill_manual(name=NULL, labels = labels, guide="legend", values=cellcols) +
    scale_colour_manual(name=NULL, labels = labels, guide="legend", values=cellcols) +
    theme_publa(base_size = figtextsize) +
    theme(legend.background = element_blank(), legend.position = "bottom",
          legend.text = element_text(size=figtextsize), plot.title = element_text(size=24, face = "bold"),
          legend.title = element_text(size=figtextsize),
          axis.line.x = element_blank(), axis.line.y = element_blank(),
          axis.ticks = element_blank(), axis.text = element_blank() 
    ) +
    guides(fill=guide_legend(keywidth=1,keyheight=1,default.unit="mm"),
           colour = guide_legend(override.aes = list(size = 2), keywidth=1,keyheight=1,default.unit="mm")) +
  labs(title = 'Weeks on high-fat diet: {round(frame_time, 0)}', x = NULL, y = NULL) +
  transition_time(time) +
  ease_aes('linear')


anim_save("AdiposeImmuneAtlas_6-12weeks_HFD.gif", p1)
  
  
  