group	gene	color	priority	T_fold
Fibro	Col1a2	tan4	1	30
Mast	Mcpt4	#C51711	3	2
Plasma	Txndc5	#BA5C7C	4	4
B	Cd79b	#B17BBC	4	5
cDC2	Cd209a	#FDB913	4	5
cDC1	Irf8	#ED6C30	6	5
DC	Naaa	darkgreen	4	2
DC	Flt3	darkgreen	4	2
Mon	Ear2	#A9C4A1	4	5
Mac	Lpl	#118437	5	4.6
Mac	Lgmn	#118437	5	3.3
Mon	Fn1	#A9C4A1	4	6
Mac	Pf4	#118437	4	13.55
Mac	Aldoa	#118437	4	10
peri Mac	Lyve1	#A9C4A1	4	1.7
Mac	Cbr2	#118437	3	3
Mac	F13a1	#118437	4	4.44
Mon	Lyz1	#A9C4A1	5	3
Mon	Retnla	#A9C4A1	3	21.3
Neut	S100a8	#EA5162	5	20
NK	Gzma	#00008b	3	10
NK	Klrb1c	#00008b	3	4
T	Cd3d	#5E83BB	4	2
ILC	Thy1	#1D50FF	2	10
ILC	Rora	#1D50FF	2	2
Endothel	Cdh5	tan1	2	2
CD4 T	Cd4	#5E83BB	1	2
CD8 T	Cd8a	#1D50FF	4	3
ILC2	Gata3	#5131BB	2	4
contam	Malat1	gray100	6	10
contam	Prrc2c	gray100	6	4.5
Gp2 DC	Gp2	#C51711	5	10
Treg	Foxp3	#B3D4F0	1	1.34
pDC	Siglech	#D8B08D	6	5
Mon	Gngt2	#A9C4A1	5	3
MonMac	C1qa	#118437	7	10