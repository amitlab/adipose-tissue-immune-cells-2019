laspotgenes <- function(plotgenes = plotgenes, id = id, fat = fat.scatter2, spec = NULL,
                             figwidth = 200/4, figheight = figwidth, figtextsize = 8, dotsize = 1, stroke = 0.05){
  
  for(gene in plotgenes){
    # or fat.scatter2 for only annotated celltypes
    spot <- data.frame(mc2@sc_x[unique(fat$CellID)], 
                       mc2@sc_y[unique(fat$CellID)],
                       mat@mat[gene,unique(fat$CellID)]
    )  
    colnames(spot) <- c("x", "y", "value")
    
    # highest points on top
    spot <- spot[order(as.numeric(factor(spot$value))),]
    
    p1 <-  
      ggplot(spot, aes(x=x, y=y, colour=log2(value+1),fill=log2(value+1))) + labs(title = paste0(gene), x = NULL, y = NULL) + 
      #      geom_point(shape=19,size=dotsize) + 
      geom_point(size=dotsize, shape=21, stroke = stroke*(figwidth/200)) +
      scale_colour_gradientn(name=NULL, breaks = c(0, floor(max(log2(na.omit(spot$value+1))))),
                             colours = colorRampPalette(#c("white", "orange", "tomato","mediumorchid4", "midnightblue"))
                               c("grey89", "#FEF8E5","#FFEABC","#FDBC52","#F68523",
                                 "#DD3226","#A31B3A","#5E2C81","#382F85","#28316C"))(1000)) +
      scale_fill_gradientn(name=NULL, breaks = c(0, floor(max(log2(na.omit(spot$value+1))))),
                           colours = colorRampPalette(#c("white", "orange", "tomato","mediumorchid4", "midnightblue"))
                             c("grey89", "#FEF8E5","#FFEABC","#FDBC52","#F68523",
                               "#DD3226","#A31B3A","#5E2C81","#382F85","#28316C"))(1000)) +
      theme_publa(base_size = figtextsize) +
      theme(legend.position = "bottom", legend.background = element_blank(), 
            legend.text = element_text(size=figtextsize), 
            legend.title = element_text(size=figtextsize), plot.title = element_text(size=figtextsize),
            axis.line.x = element_blank(), axis.line.y = element_blank(),
            axis.ticks = element_blank(), axis.text = element_blank(),
            legend.key.size = unit(1.5,"mm")
      ) + guides(colour = guide_colorbar(ticks = FALSE))
    
    p1
    
    ggsave(filename = paste0(getwd(),"/results/Spotgenes/Spotgenes_", id, "_", spec, "_" , gene,".pdf"),
           width=figwidth, height=figheight, units="mm", dpi=150, useDingbats=FALSE)
    
  }}


