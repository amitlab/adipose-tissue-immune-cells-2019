**Adipose Tissue Immune Cells 2019**

This repository contains the needed code and metadata to repeat major analyses from: 
Jaitin, Adlung, Thaiss, Weiner, Li *et al*. Cell, 2019: "Lipid-associated macrophages control metabolic homeostasis in a Trem2-dependent manner".

To run the scripts, downloaded processed files from the GSE128518 need to be added to specific folders: 
Processed UMI-tab files (AB*XXXX*.txt) from GSE128518 have to be copied into the folder: output/umi.tab.

The analysis pipeline is based on the R package "MetaCell" developed by the lab of Amos Tanay: https://bitbucket.org/tanaylab/metacell/.

Please forward any request to [Dr. Lorenz Adlung](mailto:lorenz-kurt.adlung@weizmann.ac.il).